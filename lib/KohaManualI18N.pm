package KohaManualI18N;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;
use POSIX qw( strftime );
use Array::Utils qw( intersect );
use File::Basename qw( dirname );
use File::chdir;
use File::Slurp qw( read_dir );
use IPC::Cmd qw( run );
use Locale::PO;
use JSON qw( decode_json from_json );

sub new {
    my ( $class, $params ) = @_;
    my $self = {
        project         => $params->{project},
        component       => $params->{component},
        git_branch      => $params->{git_branch},
        manual_dir      => $params->{manual_dir},
        manual_l10n_dir => $params->{manual_l10n_dir},
        lang_filter     => $params->{lang_filter},
    };
    bless( $self, $class );
    return $self;
}

sub get_langs {
    my ($self) = @_;
    my @all_langs = qw(
      ar
      cs
      de
      el
      es
      fr
      fr_CA
      hi
      it
      pt
      pt_BR
      sk
      sv
      tr
      zh_Hant
    );

    my @langs;
    if ($self->{lang_filter}) {
        my @lang_filters = split( ',', $self->{lang_filter} );
        @langs = intersect @all_langs, @lang_filters;
    }
    else {
        @langs = @all_langs;
    }
    return @langs;
}

sub run_cmd {
    my $self = shift;
    my $cmd        = shift;
    my $params     = shift;
    my $silent     = $params->{silent}     || 0;
    my $do_not_die = $params->{do_not_die} || 0;

    my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
      run( command => $cmd, verbose => !$silent );
    die sprintf "Command failed with %s\n", $error_code
      unless $do_not_die || $success;
    my $output = "@$full_buf";
    chomp $output;
    return { output => $output, error_code => $error_code };
}

sub update_git_repo {
    my ($self, $dir) = @_;
    $self->run_cmd(sprintf q{git -C %s reset --hard HEAD}, $dir);
    $self->run_cmd(sprintf q{git -C %s fetch origin}, $dir);
    $self->run_cmd(sprintf q{git -C %s clean -f -d -q}, $dir);
    $self->run_cmd(sprintf q{git -C %s checkout -B %s origin/%s}, $dir, $self->{git_branch}, $self->{git_branch});
}

sub translate_update_manual {
    my ($self) = @_;

    $CWD = $self->{manual_dir};

    $self->l("Updating koha-manual PO files");

    # Remove include:: images.rst
    # Otherwise each strings from images.rst will be present and duplicated into other rst files
    # Update - With a newer version of Sphinx the alt of the images are not picked if not included
    # However the alt is now in the .po file were it has been included, not in images.rst
    # $self->run_cmd(q{perl -p -i -n -e 's#.. include:: images.rst##' source/*.rst});

    # Remove |images|, so that they don't appear in the PO files and translators cannot mess with them
    # Update - see above, we need to include the images to make the alt translatable
    # $self->run_cmd(q{perl -p -i -n -e 's#^\s*\|(\w|-)+\|\s*$#\n#g' source/*.rst});

    # Update the pot files
    $self->run_cmd(q{make gettext});

    my @components    = $self->get_components();
    # Merge, no-wrap, sort, and remove obsoletes
    for my $lang ($self->get_langs) {
        for my $component (@components){
            $self->run_cmd(sprintf q{msgmerge --backup=none --no-wrap --sort-by-file --update locales/%s/LC_MESSAGES/%s.po build/locale/%s.pot}, $lang, $component, $component);

            $self->run_cmd(sprintf q{msgattrib --no-wrap --sort-by-file --no-obsolete locales/%s/LC_MESSAGES/%s.po -o locales/%s/LC_MESSAGES/%s.po}, $lang, $component, $lang, $component);
        }
    }

    $self->run_cmd(sprintf(q{git -C %s reset --hard HEAD}, $self->{manual_dir}));
}

sub get_components {
    my ($self) = @_;
    unless ( exists $self->{_components} ) {
        my $r = $self->run_cmd(
            sprintf(
                q{wlc --format json list-components %s}, $self->{project}
            ),
            { silent => 1 }
        );
        my $components = from_json $r->{output};
        $self->{_components} =
          [ map { $_->{is_glossary} ? () : $_->{name} || () } @$components ];
    }
    return @{ $self->{_components} };
}

sub l {
    my ($self, $msg) = @_;
    my $t = strftime( "%Y-%m-%d %H:%M:%S", localtime );
    say sprintf "[%s] %s", $t, $msg;
}

sub _lock_unlock_weblate {
    my ($self, $action ) = @_;
    for my $component ($self->get_components) {
        my $object = $self->{project} . '/' . $component;
        $self->run_cmd(qq{wlc $action $object});
    }
}

sub lock_weblate {
    my ($self) = @_;
    $self->_lock_unlock_weblate('lock');
}

sub unlock_weblate {
    my ($self) = @_;
    $self->_lock_unlock_weblate('unlock');
}

1;
