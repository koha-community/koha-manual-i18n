Koha manual I18N
================

Script to generate translated versions of the Koha manual.

The manual is generated with Sphinx.

Setup
-----

### Clone koha-manual

> git clone https://gitlab.com/koha-community/koha-manual koha-manual

### Retrieve the .po files

> cd koha-manual

> git clone https://gitlab.com/koha-community/koha-manual-l10n.git locales

### Update the .po files

This should not be needed if you are using remote branches, but if you are working locally and want to test you can update the .po files

> perl update-po.pl --manual-dir /path/to/koha-manual --manual-l10n-dir /path/to/koha-manual-l10n [--lang fr,de,es]

Outputing Docs
--------------

### For HTML
> make all_html

### For an epub
>  make all_epub

Adding a new language
---------------------

To add a new language on Weblate you will need to adjust [koha-manual-i18n](https://gitlab.com/koha-community/koha-manual-i18n.git) and [koha-manual-l10n](https://gitlab.com/koha-community/koha-manual-l10n.git).

### koha-manual-i18n

Edit `lib/KohaManualI18N.pm` and add the language code to `get_langs`. See commit "Add el (Greek)".

You need to update the code on the server.

### koha-manual-l10n
Add the .po files, first generate them:

```export LANG=lang-code
cd /path/to/koha-manual
make gettext
sphinx-intl update --line-width 999999 -p build/locale -l $LANG
cp -r locales/$LANG /path/to/koha-manual-l10n
cd /path/to/koha-manual-l10n
git add $LANG && git commit -m"Add $LANG" && git push
```

No need to update the code on the server.

### Weblate

Nothing, it will be generated during the nighly sync.

Adding a new component
----------------------

### koha-manual-i18n

In koha-manual directory:

Generate the .pot files:
`make gettext`

Remove all .pot file from build/locale/ but the new one.

Generate the .po files for all languages:

`sphinx-intl update --line-width 999999 -p build/locale -l ar -l cs -l de -l e -l es -l fr -l fr_CA -l hi -l it -l pt -l pt_BR -l sk -l sv -l tr -l zh_Hant`

Copy them to koha-manual-l10n and commit.

### Weblate

Create the new component (look at the other components and copy their settings, not "about" of course).
