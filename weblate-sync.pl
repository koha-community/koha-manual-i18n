#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Getopt::Long;
use Pod::Usage;
use File::chdir;
use FindBin;
use lib "$FindBin::Bin/lib";

use KohaManualI18N;

my ( $help, $manual_dir, $manual_l10n_dir, $lang_filter, );
GetOptions(
    'h|help'            => \$help,
    'manual-dir=s'      => \$manual_dir,
    'manual-l10n-dir=s' => \$manual_l10n_dir,
    'lang=s'            => \$lang_filter,
) || pod2usage(1);

pod2usage( -verbose => 2 ) if $help;

pod2usage(
    "manual-dir does not exist. It must point to the koha-manual repository.")
  unless $manual_dir;
pod2usage(
    "manual-l10n-dir does not exist. It must point to the koha-manual-l10n repository.")
  unless $manual_l10n_dir;

my $project = 'koha-manual';
my $git_branch = "main";

$CWD = $manual_dir;

my $i18n = KohaManualI18N->new(
    {
        project         => $project,
        git_branch      => $git_branch,
        manual_dir      => $manual_dir,
        manual_l10n_dir => $manual_l10n_dir,
        lang_filter     => $lang_filter,
    }
);

for my $dir ( ( $manual_dir, $manual_l10n_dir ) ) {
    my $r = $i18n->run_cmd( sprintf( q{git -C %s diff}, $dir ), { silent => 1 } );
    if ( $r->{output} ) {
        say sprintf "Cannot run: You have unstaged changes in %s.\nPlease commit or stash them.", $dir;
        exit 1;
    }
}

$i18n->update_git_repo($manual_dir);
$i18n->update_git_repo("$manual_dir/locales");

my @langs = $i18n->get_langs;

die "no lang or not valid lang passed" unless @langs;

$i18n->lock_weblate();

my $repo_object = $project . '/about'; # Hardcoded, 'about' is the ref component, ie. is linked to the git repo
$i18n->run_cmd(qq{wlc push $repo_object});
$CWD = $manual_l10n_dir;
$i18n->update_git_repo($manual_l10n_dir);

for my $lang_code ( sort @langs ) {
    $i18n->run_cmd(qq{cp $manual_l10n_dir/$lang_code/LC_MESSAGES/*.po $manual_dir/locales/$lang_code/LC_MESSAGES/});
}

$i18n->translate_update_manual();

for my $lang_code ( sort @langs ) {
    $i18n->run_cmd(sprintf qq{cp $manual_dir/locales/$lang_code/LC_MESSAGES/* $manual_l10n_dir/$lang_code/LC_MESSAGES/});
}

$CWD = $manual_dir;
my $r   = $i18n->run_cmd(q{git rev-parse --short HEAD}, { silent => 1 });
my $commitid = $r->{output};

$CWD = $manual_l10n_dir;
$i18n->run_cmd(qq{git commit -a -m"Update from Koha Manual - $commitid"},
  { do_not_die => 1, silent => 1 });

$i18n->run_cmd(qq{git push origin $git_branch});

$CWD = $manual_dir;
$i18n->run_cmd(q{git reset --hard HEAD});

$i18n->run_cmd(qq{wlc pull $repo_object});

END {
    $i18n->unlock_weblate();
}
